# Step 1

Intervention Mapping guides health promotion planners through the process of program design by means of a series of steps and tasks. It also provides tools and aids to structure and monitor the work during the process: matrices, tables, lists and working documents.

In the 4th edition of the Intervention Mapping textbook (Chapter 4), step 1 includes the following learning objectives and tasks:

- Establish and work with a planning group
- Conduct a needs assessment to create a logic model of the problem
- Describe the context for the intervention, including the population, setting and community
- State program goals.

In this workbook, we will do the same work. However, in the following exercises, we deviate from the structure in the book by splitting up each task into subtasks, which makes it all easier.

In this workbook, we use boxes with different colours to clearly signal types of content:

::: {.callout-note title="Guiding questions"}
Questions to guide the application of core processes (mostly brainstorming).
:::

::: {.callout-tip title="Examples"}
One or more examples to help you along.
:::

::: {.callout-important title="Products"}
One or more products, usually to complete in the exercise document, associated spreadsheets, or google sheet/presentation.
:::

Note: the `.epub` version of this workbook doesn't quite seem to be created properly yet. One problem is that these boxes do not have a color. On that note: if you happen to have expertise in this area and are willing to help out, it would be great if you would get in touch with us. The `.pdf` version seems ok.

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Task: Planning group

To ensure that you comprehensively define your problem, IM starts with a _planning group_, the first task of which is to gather data from multiple sources about the problem and its background. This is to better understand all the facets of the problem, and to estimate the feasibility of possible interventions. Usually this will be a combination of a work group and various advisory groups, consisting of members of the community, including those with the problem, often this is your target group (or for instance when you want to change behavior in children, your target group will be the parents of the at-risk group) and individuals with the relevant expertise. Here, your little group is the planning group, but we also want you to think of who would need to be in the planning group. 

### Core Processes

Who would you ask for your planning group? To help answer this question, brainstorm about questions such as:
 
::: {.callout-note title="Guiding questions"}
 
- Who represents your target population? (e.g. members of the at-risk group are important participants in a planning group)
- Who are possible environmental agents (e.g. decision makers)?
- Who are the potential program implementers (e.g. the professionals who are going to implement your program)?

:::

### Examples

::: {.callout-tip title="Examples"}

We want to increase physical activity in young children, and decrease overweight, but the at-risk group is a bit too young for the planning group, so asking parents (possible target group) is an idea.

- Prevention workers and members of the local health council (decision maker)
- The head of schools (implementers)
- Experts in physical exercise and obesity in children
- You also want people who are critical of your ideas, and who are not convinced about the intervention. They may provide you with good feedback that will eventually improve the intervention.  

:::

### Product

::: {.callout-important title="Products"}

- List the important groups of stakeholders and other populations, groups, and experts that should be included in your planning group.

:::

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Task: Conduct a needs assessment to create a logic model of the problem

Step 1 should lead to answers to a series of questions helping in producing a **logic model of the problem**: 

_**What is the problem, why is it a problem at all, since when it is a problem and for whom it is a problem, what causes the problem, who needs to be convinced this problem should be solved, who must help solve the problem, and, yes, the problem contains social psychological aspects and should potentially be solvable.**_

To answer these questions, IM begins with an analysis of community needs and capabilities. This analysis addresses people’s quality of life, health concerns and relevant behavioral and environmental conditions, as well as the community capacities that are potentially useful in improving community health. 

Brainstorm about the topic: what do you all already know about the problem? When we say 'brainstorm', we mean that you have to try to address all aspects of the problem. Usually, you ask 'what-where-why-who-for whom' questions to get to the bottom of the problem. We help you out for your first 'brainstorm'. In the box below, we listed a series of questions you can ask yourselves to get started. During your brainstorm, you can use some internet searches to get an idea of all aspects of the chosen problem.

### Core Processes

Remember that in this course, instead of applying the core processes as they are defined in Intervention Mapping, you should stick to brainstorming, supplemented by the provided literature and quick searches for more scientific literature in OpenAlex.

::: {.callout-note title="Guiding questions"}

- What is the central problem?   
- Why is it a problem? Is it a serious problem?  
- In what sense is it related to health? Who’s health?  
- For whom is it a problem (target groups; stakeholders)?   
- Which age-group (or more specific group) do you target?  
- What behaviors are related to the problem, or cause the problem?   
- What are the environmental conditions or conditions that are related to the problem?
- Who are the environmental agents who can control the environmental conditions, and what do they need to do to change those conditions?
- What are the determinants or correlates of the behaviors involved, both for the target population, and for the environmental agents?
- Whose cooperation is necessary to help solve the problem? 
- Describe the context: How does the environment directly or indirectly contribute to the problem?  
- What are the behaviors of the at-risk group?
- What environmental conditions contribute to the problem (at the interpersonal, organizational, communal and societal level)?
- What would be the best intervention setting?
- Who is the at-risk group?
- Who is the target group (can be the same as the at-risk group)?

:::

### Examples

::: {.callout-tip title="Examples"}

Example: We want to promote social distancing in Amsterdam and we want to target the older population.

- At-risk group = 60+ years old citizen
- Behavior at risk group = They do not always keep the proper distance when in the streets 
- Target group = 60+ years old citizen
- Environmental conditions = the sidewalks are not wide enough
- Environmental agents = the municipality

Example: We want primary school children to exercise more frequently.

- At-risk group = primary school children 
- Behavior at risk group = They play videogames all afternoon. 
- Target group = parents
- Environmental conditions = no possibility to play outside
- Environmental agents = heads of schools 

:::

### Product

::: {.callout-important title="Products"}

- List at least four determinants.
- List at least four environmental conditions.
- Produce a diagram that visualises the logic model of the problem.

:::

![Logic model of the problem, see book p. 227.](img/LogicModel.png)

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Task: Describe the context for the intervention, including the population, setting and community

In step 1, you don't only map out the problems and their causes, but you also map out your target population's landscape more generally. Specifically, it's important to identify in which setting the problem occurs (i.e. also aspects that may not play a role for the problem), understand the population (e.g. culture, existing communities, etc), and other assets that you may be able to involve in the intervention later on. This is broadly referred to as the asset assessment. The result is a description of your target population and their surroundings.

This also includes the asset assessment, where you map the specific strenghts of the population you want to intervene in. Assets are often mapped for four environment levels: the social environment, the information environment, the policy environment, and the physical environment. Keep in mind that assets are specific characteristics that are not generally true, but specific to your target population (e.g. exceptional infastructure (physical environment), closely knit community bonds (social environment), a specific social medium that is widely followed (information environment), or good adjustment of policies to local needs (policy environment)).

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->

## Task: Stating program goals 

Determine your program goals. You prioritize the potential goals on the basis of the needs assessment (see p. 254). This includes deciding what behaviors or environmental conditions are _most relevant_ and _most likely to change_. 

State in one sentence the expected change of your intervention and its time frame in clear quantifiable terms.

Distinguish the following types of goals:

- Ultimate program goals: these typically involve the quality of life or health of a specific population (the at-risk population)
- Environmental program goals: these are the environmental conditions that you want to change in your program.
- Behavioral program goals: these are the behaviors of the target population you want to change (note that the target population can be different from the at-risk population)

A program always has one or more ultimate goals. The environmental goals and behavioral goals are means to achieve those ultimate goals, but it is possible a program only has environmental goals, or only behavioral goals.^[Note that in step 2, each environmental goal will be analysed and will result in behavioral goals for the environmental agents that have agency over the relevant environmental conditions.]

Note that you may want to change behaviors of environmental agents, too. However, in that case, often those _behaviors_ are in fact not what is important: instead the environmental condition that is caused or maintained by their behavior is important. That environmental condition (e.g. a certain norm) is what influences the problem(s) you want to address, so often you are better off specifying you want to change that environmental condition. In step 2, you will identify which behaviors each environmental agent needs to change in order to change the environmental condition.

### Program Goal

::: {.callout-note title="Guiding questions"}

- Which specific target group do we choose? 
- What is the current state (e.g. behavior, outcomes)?
- How much behavior change can we expect from our target group? 
- How quickly (or in what timeframe) would it be possible to reach that goal? Is this very likely?
- Are effects on relevant health outcomes quantifiable within the chosen relevant and realistic time frame or should we choose a certain behavior or environment?

:::

Note that selecting goals is not a purely theory- and evidence-based decision. It often requires compromises between several interests. For example, there are often political interests. Funders often want to see a certain problem addressed, and may prefer evaluating that problem specifically; or on the other hand, you may collaborate with governmental organisations that want (or don't want) certain outcomes to be selected.

Simultaneously, there are methodological considerations: can that problem be measured well? What effect size would you expect, and is that sample size reasonable? Other considerations are theoretical: how quickly do you expect that a potential outcome can change if your intervention is successful? This is often combined with pragmatic considerations (e.g. how long will you have for the evaluation), and sometimes activistic considerations play a role, too (e.g. you might collaborate with a volunteer foundation that prioritizes certain outcomes to enhance visibility).

Finally, in the goal, include both the present state (the 'baseline') as well as the desired result.

### Examples

::: {.callout-note title="Examples"}

Reaching the 60+ population is more difficult, and they are physically slower, so although they are the at-risk group, we now choose the younger adults as target group. To do this in the entire city is not feasible, so we focus at one busy street. To set our goals too far in the future would be useless, so we want fast implementation and fast results. The program goal would therefore be *to increase social distancing of adults under 30 from 60% to 80% on the sidewalk in the Kalverstraat in Amsterdam within 2 weeks.*

- We wanted primary school children to exercise more frequently. We chose the following program goals: 
    - *to increase physical activity in children between 6 and 10 from an average of by 26 minutes per week to 52 minutes per week and to decrease weight in the overweight children from an average of 72kg to 65kg, following 2 years of our intervention at the schools in Amsterdam-Noord.* 
    - *to decrease weight in the overweight children between 6 and 10 from an average of 72kg to 65kg, following 2 years of our intervention at the schools in Amsterdam-Noord.*

Note that we separated increasing activity and decreasing weight. Here we might picked the parents as the target group (we need them to bring the children to the extracurricular gymnastic classes) or the heads of schools in Amsterdam-Noord (who need to implement our intervention program at their school). By the way: the program goals should be feasible! Our examples might not be that feasible, but at least we focused at a specific age group of children in a specific area (Amsterdam-Noord), instead of 'all children in the world'.  

:::

### Products

Although "in real life", it is quite possible to not have any behavioral program goals, when working with this workbook, you *must* select a target group (which can be either the at-risk group or a different population) and a behavioral goal.

::: {.callout-important title="Products"}

- List the ultimate program goal(s).
- List the environmental program goal(s), if any.
- List the behavioral program goal(s).

:::
